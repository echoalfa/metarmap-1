import os
import time
from datetime import datetime
import git
import json
from flask import Flask, render_template, request, session, redirect
from passlib.hash import sha256_crypt

dirname, scriptname = os.path.split(os.path.abspath(__file__))
THIS_DIRECTORY = f'{dirname}{os.sep}'
CONFIG_FILE = f'{THIS_DIRECTORY}METARMap{os.sep}config.json'
CUSTOMER_FILE = f'{THIS_DIRECTORY}ui_config.json'


DEBUG = bool(os.path.exists(f'{THIS_DIRECTORY}debug'))

if DEBUG:
    WPA_SUPPLICANT = 'wpa_supplicant.conf'
else:
    WPA_SUPPLICANT = '/etc/wpa_supplicant/wpa_supplicant.conf'

app = Flask(__name__)
app.config['SECRET_KEY'] = 'RmxvY2tTaHJlZGRpbmdMdWxsYWJ5Q291cmllcg=='

@app.route('/', methods=['GET', 'POST'])
def initail_setup():
    message = ''
    submitted = ''
    error = ''
    if request.method == 'POST':
        if all([request.form['password'], request.form['email'], request.form['ssid'], request.form['key']]):
            owner = load_json(CUSTOMER_FILE)
            if request.form['password'] == owner['customer']['password']:
                add_network(request.form['ssid'], request.form['key'] )
                add_email(request.form['email'])
                submitted = {
                    'email':request.form['email'],
                    'ssid':request.form['ssid'],
                    'network_key':request.form['key']
                }
                message = 'Details registered. If you\'re happy with these settings, press the button below to complete the setup and restart your METARMap'
            else:
                error = 'That password was not recognised. Please refer to the email sent at the time of purchase.'
        else:
            error = 'Please provide all the required details and try again.'
    return render_template(
        'setup.html',
        message = message,
        error = error,
        submitted = submitted
    )


@app.route('/metarmap', methods=['GET', 'POST'])
def authenticate():
    message = ''
    if 'authenticated' not in session:
        if request.method == 'POST':
            if request.form['password'] is not None:
                owner = load_json(CUSTOMER_FILE)
                if request.form['password'] == owner['customer']['password']:
                    session['authenticated'] = True
                    if 'name' in owner:
                        name = owner['customer']['name']
                        if name == '':
                            name = 'METAR Map'
                    else:
                        name = 'METAR Map'
                    return render_template(
                        'index.html',
                        name = name
                    )
                else:
                    message = 'That password was not recognised. Please refer to the email sent at the time of purchase.'
            else:
                message = 'Please log in with the password provided after purchasing the map'
        return render_template(
            'login.html',
            message = message
        )
    else:
        return render_template(
            'index.html'
        )

@app.route('/metarmap/display', methods=['GET', 'POST'])
def configure():
    if 'authenticated' in session:
        config = load_json(CONFIG_FILE)
        if request.method == 'POST':
            for option in config['colours']:
                config['colours'][option] = [int(value) for value in request.form[f'colour_{option}'].split(',')]

            if request.form['wind_enabled'] == 'true':
                config['wind']['enabled'] = True
            else:
                config['wind']['enabled'] = False
            config['wind']['blink_threshold'] = int(request.form['blink_threshold'])
            config['wind']['high_wind_threshold'] = int(request.form['high_wind_threshold'])

            if request.form['lightning_enabled'] == 'true':
                config['lightning']['enabled'] = True
            else:
                config['lightning']['enabled'] = False

            config['brightness']['day'] = float(request.form['day_brightness'].split('.')[0]) / 100
            config['brightness']['night'] = float(request.form['night_brightness'].split('.')[0]) / 100

            save_json(CONFIG_FILE, config)
            time.sleep(1)
            os.system(f'sudo pkill -f {THIS_DIRECTORY}METARMap/metar.py')
            time.sleep(1)
            os.system(f'{THIS_DIRECTORY}METARMap/refresh.sh')

        colours = {}
        for option in config['colours']:
            colours[option] = ','.join( [str(col) for col in config['colours'][option]] )

        return render_template(
            'display.html',
            colours = colours,
            wind = config['wind'],
            lightning = config['lightning'],
            brightness = config['brightness'],
            dim_at_sunset = config['dim_at_sunset']
        )
    else:
        return redirect('/metarmap')

@app.route('/metarmap/connectivity', methods=['GET', 'POST'])
def connectivity():
    if 'authenticated' in session:
        settings, network_strings = parse_conf()

        changes_made = False
        customer = load_json(CUSTOMER_FILE)

        networks = []
        for network in network_strings:
            parsed_network = parse_network(network)
            if parsed_network['ssid'] != '"Metarmap"':
                networks.append(parsed_network)

        if request.method == 'POST':
            if 'delete_ssid'in request.form:
                network = [n for n in networks if request.form['delete_ssid'] in n['ssid']][0]
                delete_network( network )
                changes_made = True

            if 'add_ssid' in request.form:
                if request.form['add_ssid'] is not None and request.form['add_ssid'] != '':
                    add_network(request.form['add_ssid'], request.form['add_password'] )
                    changes_made = True

            if 'email' in request.form:
                customer['customer']['email'] = request.form['email']
                save_json(CUSTOMER_FILE, customer)
                changes_made = True

            settings, network_strings = parse_conf()

            networks = []
            for network in network_strings:
                parsed_network = parse_network(network)
                if parsed_network['ssid'] != '"Metarmap"':
                    networks.append(parsed_network)

        return render_template(
            'connectivity.html',
            networks = networks,
            changes_made = changes_made,
            email = customer['customer']['email']
        )
    else:
        return redirect('/metarmap')

@app.route('/metarmap/update', methods=['GET'])
def update():
    # === Work in progress ===
    repo = git.Repo(THIS_DIRECTORY)
    before = list(repo.iter_commits("master", max_count=5))
    repo.remotes.origin.pull()

    after = list(repo.iter_commits("master", max_count=5))

    change_log = [ [commit.committed_datetime.strftime('%Y-%m-%d'), commit.message.strip()] for commit in after ]
    if before == after:
        message = 'The system is already up to date, but here were the latest changes:'
        for commit in after:
            print(commit.message)
    else:
        message = 'Update complete. Click the button below to apply the following changes and restart your map.'

    return render_template(
        'update.html',
        message = message,
        changes = before != after,
        change_log = change_log
    )


@app.route('/reboot', methods=['GET', 'POST'])
def reboot():
    if request.args.get("new_network"):
        customer = load_json(CUSTOMER_FILE)
        customer['environment']['new_network'] = True
        save_json(CUSTOMER_FILE, customer)

    if DEBUG:
        print('=== reboot ===')
    else:
        time.sleep(2)
        os.system('sudo reboot')
    return redirect('/metarmap')

# === NON-WEB FUNCTIONS ==

def delete_network(network):
    remove_network = f'ssid={network["ssid"]}\nscan_ssid={network["scan_ssid"]}\npsk={network["psk"]}\nkey_mgmt={network["key_mgmt"]}'
    remove_network = 'network={\n' + remove_network + '\n}'
    with open(WPA_SUPPLICANT, 'r', encoding='utf-8') as f:
        networks = f.read()
    networks = networks.replace(remove_network, '')
    with open(WPA_SUPPLICANT, 'w', encoding='utf-8') as f:
        f.write(networks)


def add_network(ssid, password):
    new_network = f'ssid="{ssid}"\nscan_ssid=1\npsk="{password}"\nkey_mgmt=WPA-PSK'
    new_network = 'network={\n' + new_network + '\n}'

    with open(WPA_SUPPLICANT, 'r', encoding='utf-8') as f:
        networks = f.read()
    networks = f'{networks}\n\n{new_network}'
    with open(WPA_SUPPLICANT, 'w', encoding='utf-8') as f:
        f.write(networks)

def parse_network(network_string):
    network = {}
    for line in network_string.split(os.linesep):
        if '=' in line:
            key, value = line.split('=')
            if key != 'network':
                network[key.strip()] = value.strip()
    return network

def add_email(email):
    customer = load_json(CUSTOMER_FILE)
    customer['customer']['email'] = email
    save_json(CUSTOMER_FILE, customer)

def parse_conf():
    content = load_file(WPA_SUPPLICANT)
    content = content.split('\n')
    other_settings = []
    all_netwroks = []
    network = []
    network_entry = False
    for line in content:
        if line.startswith('network'):
            network_entry = True
        if network_entry:
            network.append(line)
            if '}' in line:
                network_entry = False
                all_netwroks.append(os.linesep.join(network))
                network = []
        else:
            other_settings.append(line)
    return os.linesep.join(other_settings), all_netwroks


def load_file(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        return f.read()

def save_file(filename, data):
    with open(filename, 'w', encoding='utf-8') as f:
        f.write(data)

def load_json(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        data = json.loads(f.read())
        return data

def save_json(filename, data):
    with open(filename, 'w', newline='', encoding='utf-8') as f:
        f.write(json.dumps(data, indent=4))

if __name__ == '__main__':
    if DEBUG:
        app.run( debug=True )
    else:
        app.run( port=80, host="0.0.0.0", )
