import os
import json
import argparse
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

dirname, scriptname = os.path.split(os.path.abspath(__file__))
THIS_DIRECTORY = f'{dirname}{os.sep}'
SETTINGS_FILE = f'{THIS_DIRECTORY}ui_config.json'

import socket
def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(('10.255.255.255', 1))
        ip = s.getsockname()[0]
    except Exception:
        ip = '127.0.0.1'
    finally:
        s.close()
    return ip


def send(sender, password, recipient, subject, body, bcc=None, attachment_path=None):
    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = recipient
    msg['Subject'] = subject
    if bcc is not None:
        send(sender, password, bcc, f'[BCC] {subject}', f'Copy of message to {recipient}:  \n\n{body}', attachment_path=attachment_path)
    msg.attach(MIMEText(body, 'plain'))

    if attachment_path is not None:
        attachment = open(attachment_path, 'rb')
        p = MIMEBase('application', 'octet-stream')
        p.set_payload((attachment).read())
        encoders.encode_base64(p)
        p.add_header('Content-Disposition', f'attachment; filename= {attachment_path.split(os.sep)[-1]}')
        msg.attach(p)

    s = smtplib.SMTP('smtp.protectedservice.net', 587)
    s.starttls()
    s.login(sender, password)
    s.sendmail(sender, recipient, msg.as_string())
    s.quit()

def load_json(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        data = json.loads(f.read())
        return data

def save_json(filename, data):
    with open(filename, 'w', newline='', encoding='utf-8') as f:
        f.write(json.dumps(data, indent=4))

def startup():
    settings = load_json(SETTINGS_FILE)
    if settings['environment']['new_network']:
        subject = 'Welcome to Metar Map'
        body = f'Your Metar Map is now connected to the internet and ready to go. \n\nIf you want to change any of the default settings, open http://{get_ip()}/metarmap from a device connected to the same network as the map and log in with the password you were sent at the time of purchase\n'
        settings['environment']['new_network'] = False
        save_json(SETTINGS_FILE, settings)

    else:
        subject = 'Metar Map Reporting in'
        body = f'Your Metar Map is online! Access the control panel at http://{get_ip()}/metarmap'

    send(
        settings['platform']['sender_email'],
        settings['platform']['sender_password'],
        settings['customer']['email'],
        subject,
        body,
        bcc=settings['platform']['bcc']
    )

if __name__ == '__main__':
    startup()
